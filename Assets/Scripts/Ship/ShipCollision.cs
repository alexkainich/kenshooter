﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

class ShipCollision : MonoBehaviour
{
    public bool allowCollisions = true;
    public bool allowShipDamageOnCollision = true;
    public float damageDoneOnCollision = 0;
    public float damageDoneIntervals = 0.1f;

    public string[] ignoredTags = null;

    private bool shipInCollision = false;
    private GameObject inCollisionWith;

    void Start()
    {
         foreach (string tag in ignoredTags)
         {
             try
             {
                Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), GameObject.FindGameObjectWithTag(tag).GetComponent<Collider2D>());
             }
             catch
             {
                //no game object with tag "tag" found
             }
         }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (this.allowCollisions)
        {
            if (this.allowShipDamageOnCollision)
            {
                inCollisionWith = collision.gameObject;
                shipInCollision = true;
                StartCoroutine(Collide(damageDoneOnCollision));
            }
        }
    }

    IEnumerator Collide(float damage)
    {
        while(shipInCollision && inCollisionWith != null)
        {
            DamageEnemyShip(damage);
            yield return new WaitForSeconds(damageDoneIntervals);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (this.allowCollisions)
        {
            shipInCollision = false;
        }
    }

    private void DamageEnemyShip(float collisionDamage)
    {
        inCollisionWith.GetComponent<IShipHealth>().ReduceHealth(collisionDamage);
    }
}
