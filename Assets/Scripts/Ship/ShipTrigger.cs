﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

class ShipTrigger : MonoBehaviour
{
    public bool allowDamageFromTriggers = true;
    public string[] ignoredTags = null;

    private bool inTrigger = false;
    private GameObject inTriggerWith;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (this.allowDamageFromTriggers && !ignoredTags.Contains(other.gameObject.tag))
        {
            var shotProperties = other.gameObject.GetComponent<ShotProperties>();
            inTriggerWith = other.gameObject;
            inTrigger = true;

            var damage = shotProperties.damage;
            var damageEverySeconds = shotProperties.damageEverySeconds;
            var pushBackForce = shotProperties.pushBackforce;

            if (damage != 0) { StartCoroutine(Trigger(damage, damageEverySeconds)); }
            if (pushBackForce != 0) { StartCoroutine(GetPushedBack(pushBackForce)); }
        }
    }

    IEnumerator Trigger(float damage, float damageEverySeconds)
    {
        while(inTrigger && inTriggerWith != null)
        {
            if (damage != 0) { TakeDamage(damage); }
            yield return new WaitForSeconds(damageEverySeconds);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (this.allowDamageFromTriggers)
        {
            inTrigger = false;
        }
    }

    private void TakeDamage(float triggerDamage)
    {
        gameObject.GetComponent<IShipHealth>().ReduceHealth(triggerDamage);
    }


    IEnumerator GetPushedBack(float pushBackForce)
    {
        while (inTrigger && inTriggerWith != null)
        {
            gameObject.GetComponent<IShipControl>().PushBackShip(pushBackForce, (Vector2)inTriggerWith.transform.position);
            yield return new WaitForSeconds(0);
        }
    }
}
