﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipShield : MonoBehaviour
{

	private IShipHealth shipHealthScript;
    private float shipCurrentHealth;
	private float shipPreviousHealth;

    private SpriteRenderer spriteRenderer;

	void Start()
	{
		shipHealthScript = gameObject.transform.parent.gameObject.GetComponent<IShipHealth>();
		shipCurrentHealth = shipHealthScript.CurrentHealth;
		shipPreviousHealth = shipCurrentHealth;

        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
    }

    void Update()
    {
        if (shipCurrentHealth < shipPreviousHealth)
        {
            StartCoroutine(FlashShield(1));
        }
		shipPreviousHealth = shipCurrentHealth;
		shipCurrentHealth = shipHealthScript.CurrentHealth;
	}

    IEnumerator FlashShield(int flashCount)
    {
        for (int i = 0; i < flashCount; i++)
        {
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(0.1f);
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
