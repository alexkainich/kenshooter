﻿using UnityEngine;
using System.Collections;

interface IShipControl
{
    void PushBackShip(float collisionForce, Vector2 pusherPosition);
}
