﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

interface IShipHealth
{
    void ReduceHealth (float amount);

    float CurrentHealth { get; }
}

