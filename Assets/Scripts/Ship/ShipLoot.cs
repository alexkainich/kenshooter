﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipLoot : MonoBehaviour
{

    public float minXOnDeath;
    public GameObject Loot;
    public int howMany;

    private IShipHealth shipHealth;
    private int LootDropped;
    void Start()
    {
        shipHealth = gameObject.GetComponent<IShipHealth>();
        LootDropped = 0;
    }

    void Update()
    {
        if (transform.position.x > minXOnDeath && shipHealth.CurrentHealth <= 0)
        {
            while (LootDropped < howMany)
            {
				Vector3 randomVector = new Vector3(Random.Range(0.0f, 3.0f), Random.Range(0.0f, 3.0f), 0);
                GameObject loot = Instantiate(Loot, gameObject.transform.position + randomVector, Quaternion.identity);
                Destroy(loot, 10f);
                LootDropped ++;
            }
        }
    }
}

