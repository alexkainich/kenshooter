﻿using UnityEngine;
using System.Collections;

public class Player2Control : MonoBehaviour, IShipControl
{
    public float speed;
    public float smoothing;

    public Boundary boundary;
    [System.Serializable]
    public class Boundary
    {
        public float xMin, xMax, yMin, yMax;
    }
    private float normalSpeed;
    private Vector2 currentSpeed;

    private GameObject engines;

    private void Awake()
    {
        normalSpeed = speed;
        engines = transform.Find("ShipEngines").gameObject;

        DontDestroyOnLoad(gameObject);
    }

    void FixedUpdate()
    {
        Rigidbody2D rgb = GetComponent<Rigidbody2D>();

        float moveHorizontal = Input.GetAxis("Horizontal2");
        float moveVertical = Input.GetAxis("Vertical2");
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        float newManeuverX = Mathf.MoveTowards(rgb.velocity.x, movement.x * speed, smoothing);
        float newManeuverY = Mathf.MoveTowards(rgb.velocity.y, movement.y * speed, smoothing);

        rgb.velocity = new Vector2(newManeuverX, newManeuverY);
        rgb.position = new Vector2
        (
            Mathf.Clamp(rgb.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(rgb.position.y, boundary.yMin, boundary.yMax)
        );

        if (Input.GetButton("SpeedBoost2"))
        {
            speed = normalSpeed + 10;
            engines.SetActive(true);
        }
        else
        {
            speed = normalSpeed;
            engines.SetActive(false);
        }

        currentSpeed = rgb.velocity;
    }

    public void PushBackShip(float collisionForce, Vector2 pusherPosition)
    {
        Vector2 forceVector = ((Vector2)gameObject.transform.position - pusherPosition).normalized;
        GetComponent<Rigidbody2D>().velocity = currentSpeed + (forceVector * collisionForce);
        currentSpeed = GetComponent<Rigidbody2D>().velocity;
    }
}
