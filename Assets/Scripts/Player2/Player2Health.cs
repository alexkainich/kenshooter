﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


//Perhaps this class cannot be generalised as it is very specific to the player
//It needs acces to the Canvas and to PlayerControl etc
public class Player2Health : MonoBehaviour, IShipHealth
{
    public float startingHealth = 100.0f;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);

    Animator anim;
    AudioSource playerAudio;
    Player2Control player2Control;
    private bool isDead;
    private bool shipDamaged;
    [HideInInspector]
    public float CurrentHealth { get; private set; }

    void Awake()
    {
        //anim = GetComponent<Animator>();
        //playerAudio = GetComponent<AudioSource>();
        player2Control = GetComponent<Player2Control>();
        //playerShooting = GetComponentInChildren<PlayerShooting>();
        this.CurrentHealth = startingHealth;
    }


    void Update()
    {
        if (shipDamaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        shipDamaged = false;
    }

    public void ReduceHealth(float amount)
    {
        shipDamaged = true;

        CurrentHealth -= amount;

        healthSlider.value = CurrentHealth;

        if (CurrentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    void Death()
    {
        isDead = true;

        //playerControl.DisableEffects();

        //anim.SetTrigger("Die");

        //playerAudio.clip = deathClip;
        //playerAudio.Play();

        player2Control.enabled = false;
        Destroy(gameObject, 3f);
    }


}