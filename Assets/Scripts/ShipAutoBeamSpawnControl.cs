﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ShipAutoBeamSpawnControl : MonoBehaviour
{

    public GameObject beamPrefab;
    public GameObject explosion;
    public bool autoAssignBeamsExplosionCollisionLayer = true;
    public float damage;
    public float fireRate;
    public float weaponRadius;
    public LayerMask layersToTarget;
    public string[] ignoredTags;

    private bool fired;
    private float nextFire;
    private List<Collider2D> possibleTargets;
    private CircleCollider2D beamCollider;

    void Start()
    {
        this.possibleTargets = new List<Collider2D>();
    }

    void Update()
    {
        Destroy(beamCollider);
        this.possibleTargets = Physics2D.OverlapCircleAll(gameObject.transform.position, weaponRadius, layersToTarget).ToList();
        if (possibleTargets.Any() && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            FireBeam();
        }
    }

    public void FireBeam()
    {
        possibleTargets.OrderBy(t => Vector2.Distance(gameObject.transform.position, t.transform.position));
        var chosenTarget = possibleTargets[0].transform.position;

        InstantiateBeamWithColliderAndExplosion(chosenTarget);
    }

    private void InstantiateBeamWithColliderAndExplosion(Vector2 target)
    {
        var beamPoints = new Vector3[2];
        beamPoints[0] = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 0f);
        beamPoints[1] = new Vector3(target.x, target.y, 0f);

        var beam = Instantiate(beamPrefab, beamPrefab.transform.position, Quaternion.identity);
        beam.GetComponent<LineRenderer>().SetPositions(beamPoints);
        beam.GetComponent<ShotProperties>().damage = this.damage;

        beamCollider = beam.AddComponent<CircleCollider2D>();
        beamCollider.radius = 1;
        beamCollider.isTrigger = true;
        beamCollider.offset = new Vector2(beamPoints[1].x, beamPoints[1].y);

        InstantiateExplosion(beamPoints);

        Destroy(beam, 0.05f);
    }

    private void InstantiateExplosion(Vector3[] beamPoints)
    {
        var explosionObj = Instantiate(explosion, beamPoints[1], Quaternion.identity);
        
        if (autoAssignBeamsExplosionCollisionLayer) 
        { 
            if (gameObject.layer == LayerMask.NameToLayer("Player"))
                explosionObj.layer =  LayerMask.NameToLayer("PlayerExplosion");
            else if (gameObject.layer == LayerMask.NameToLayer("Enemy"))
                explosionObj.layer =  LayerMask.NameToLayer("EnemyExplosion");
        }

        float explosionClipLength = explosionObj.GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length;
        Destroy(explosionObj, explosionClipLength);
    }
}