﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyShotSpawnControl : MonoBehaviour
{
    public bool fireOnlyOnce = false;
    public Vector2 attackWait;
    public GameObject shot;
    public float rotateSpriteZ;
    public ShotControl.ShotDirection shotDirection;
    public bool targetPlayer;
    public FacingDirection targetPlayerShotDirection = FacingDirection.LEFT;
    public bool asssignEnemyShotCollisionLayer;

    private GameObject[] players;

    void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        int noOfPlayers = players.Where(player => player != null).Count();
        if (noOfPlayers == 2)
        {
            while (players.Where(player => player != null).Count() != 0 && players.Where(player => player != null).Select(player => player.transform.position.x).OrderBy(x => x).First() < transform.position.x)
            {
                yield return new WaitForSeconds(Random.Range(attackWait.x, attackWait.y));
                GameObject shotPrefab = Instantiate(shot, gameObject.transform.position, gameObject.transform.rotation);
                if (rotateSpriteZ != 0) { shotPrefab.transform.Rotate(0f, 0f, rotateSpriteZ); }
                if (asssignEnemyShotCollisionLayer) { shotPrefab.layer = LayerMask.NameToLayer("EnemyShot"); }

                if (targetPlayer)
                {
                    if (players.Where(player => player != null).Any())
                        shotPrefab.transform.rotation = FaceObject(shotPrefab.transform.position, players.Where(player => player != null).Select(player => player.transform.position).OrderBy(x => x.x).Last(), targetPlayerShotDirection);
                }

                shotPrefab.GetComponent<ShotControl>().SetVelocity(shotDirection);
                if (fireOnlyOnce) { yield break; }
            }
        }
        else
        {
            while (players.Any() && players[0] != null && players[0].transform.position.x < transform.position.x)
            {
                yield return new WaitForSeconds(Random.Range(attackWait.x, attackWait.y));
                GameObject shotPrefab = Instantiate(shot, gameObject.transform.position, gameObject.transform.rotation);
                if (rotateSpriteZ != 0) { shotPrefab.transform.Rotate(0f, 0f, rotateSpriteZ); }
                if (asssignEnemyShotCollisionLayer) { shotPrefab.layer = LayerMask.NameToLayer("EnemyShot"); }

                try
                {
                    if (targetPlayer)
                    {
                        if (players[0] != null)
                            shotPrefab.transform.rotation = FaceObject(shotPrefab.transform.position, players[0].transform.position, targetPlayerShotDirection);
                    }
                }
                catch
                {
                    //players not found
                }

                shotPrefab.GetComponent<ShotControl>().SetVelocity(shotDirection);
                if (fireOnlyOnce) { yield break; }
            }
        }
    }

    public enum FacingDirection
    {
        UP = 270,
        DOWN = 90,
        LEFT = 180,
        RIGHT = 0
    }

    private static Quaternion FaceObject(Vector2 startingPosition, Vector2 targetPosition, FacingDirection facing)
    {
        Vector2 direction = targetPosition - startingPosition;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        angle -= (float)facing;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
