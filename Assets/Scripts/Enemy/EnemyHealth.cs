﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHealth : MonoBehaviour, IShipHealth
{
    public float startingHealth;
    public GameObject shipExplosion;
    public bool autoAssignShipsExplosionCollisionLayer = true;
    public bool repeatedExplosionsOnDeath;
    public bool destroyChildOnHalfHealth;

    [HideInInspector]
    public float CurrentHealth { get; private set; }

    private int startingChildrenCount;

    void Awake()
    {
        CurrentHealth = startingHealth;
        startingChildrenCount = gameObject.transform.childCount;
    }


    void Update()
    {

    }


    public void ReduceHealth(float amount)
    {
        CurrentHealth -= amount;

        if (destroyChildOnHalfHealth && CurrentHealth <= startingHealth/2 && gameObject.transform.childCount >= startingChildrenCount)
        {
            DestroyRandomChild();
        }

        if (CurrentHealth <= 0)
        {
            if (repeatedExplosionsOnDeath)
            {
                InvokeRepeating("BossExplosions", 0, 0.2f);
                DeactiveAllChildren();
                Destroy(gameObject, 3);
            }
            else
            {
                InstantiateExplosion();

                Destroy(gameObject);
            }
        }
    }

    private void InstantiateExplosion()
    {
        var shipExpl = Instantiate(shipExplosion, gameObject.transform.position, Quaternion.identity);

        if (autoAssignShipsExplosionCollisionLayer) 
        { 
            if (gameObject.layer == LayerMask.NameToLayer("Player"))
                shipExpl.layer =  LayerMask.NameToLayer("PlayerExplosion");
            else if (gameObject.layer == LayerMask.NameToLayer("Enemy"))
                shipExpl.layer =  LayerMask.NameToLayer("EnemyExplosion");
        }

        float explosionClipLength = shipExpl.GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length;
        Destroy(shipExpl, explosionClipLength);
    }

    private void DestroyRandomChild()
    {
        var randomChildIndex = Random.Range(0, gameObject.transform.childCount - 1);
        var child = gameObject.transform.GetChild(randomChildIndex).gameObject;
        if (child != null)
        {
            Destroy(child);
            InvokeRepeating("SpawerExplosions", 0, 0.5f);
        }
    }

    public void DeactiveAllChildren()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            var child = gameObject.transform.GetChild(i).gameObject;
            if (child != null)
                child.SetActive(false);
        }
    }

    public void BossExplosions()
    {
        var shipExpl = Instantiate
        (
            shipExplosion,
            new Vector2(gameObject.transform.position.x + Random.Range(-2.0f, 2.0f), gameObject.transform.position.y + Random.Range(-2.0f, 2.0f)),
            Quaternion.identity
        );
        Destroy(shipExpl, 2f);
    }

    public void SpawerExplosions()
    {
        var shipExpl = Instantiate
        (
            shipExplosion,
            new Vector2(gameObject.transform.position.x, gameObject.transform.position.y),
            Quaternion.identity
        );
        Destroy(shipExpl, 2f);
    }
}