﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyControl : MonoBehaviour, IShipControl
{

    public float smoothing;
    public float speed;
    public ShipDirection shipDirection;
    public Vector2 startWait;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;
    public Boundary boundary;

    private Transform playerTransform;
    private Vector2 targetSpeed;
    private Vector2 currentSpeed;
    private float targetManeuver;
    private Rigidbody2D rb;
    private GameObject[] players;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        players = GameObject.FindGameObjectsWithTag("Player");
        playerTransform = (players.Any() && players[0] != null) ? players[0].transform : null;

        switch (shipDirection)
        {
            case ShipDirection.right:
                rb.velocity = GetComponent<Transform>().right * speed;
                break;
            case ShipDirection.left:
                rb.velocity = - GetComponent<Transform>().right * speed;
                break;
            case ShipDirection.up:
                rb.velocity = GetComponent<Transform>().up * speed;
                break;
            case ShipDirection.down:
                rb.velocity = - GetComponent<Transform>().up * speed;
                break;
        }

        targetSpeed = rb.velocity;
        currentSpeed = targetSpeed;

        if (players.Any() && players.Where(player => players != null).Any())
            StartCoroutine(Evade());
    }

    // Evade method
    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

        while (players.Where(players => players != null).Any())
        {
            playerTransform = players.Where(player => player != null).Select(transform => transform.transform).OrderBy(x => x.transform.position.x).Last();

            targetManeuver = playerTransform.position.y;
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float newManeuverX = Mathf.MoveTowards(rb.velocity.x, targetSpeed.x, Time.deltaTime * smoothing);
        float newManeuverY = Mathf.MoveTowards(rb.velocity.y, targetManeuver, Time.deltaTime * smoothing);

        rb.velocity = new Vector2(newManeuverX, newManeuverY);
        rb.position = new Vector2
            (
                Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
                Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax)
            );

        if (rb.position.x < -GameObject.FindWithTag("SceneBorder").GetComponent<BoxCollider2D>().bounds.extents.x +GameObject.FindWithTag("SceneBorder").GetComponent<BoxCollider2D>().bounds.center.x)
        {
            Destroy(gameObject);
        }
    }

    public void PushBackShip(float collisionForce, Vector2 pusherPosition)
    {
        Vector2 forceVector = ((Vector2)gameObject.transform.position - pusherPosition).normalized;
        gameObject.GetComponent<Rigidbody2D>().velocity = currentSpeed + (forceVector * collisionForce);
    }

    public enum ShipDirection
    {
        right = 1,
        left = -1,
        up = 2,
        down = -2
    }
}
