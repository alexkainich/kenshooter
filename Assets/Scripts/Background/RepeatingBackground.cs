﻿using UnityEngine;
using System.Collections;

public class RepeatingBackground : MonoBehaviour 
{
    private BoxCollider2D backgroundCollider;
    private float backgroundHorizontalLength;

    //Awake is called before Start.
    private void Awake ()
    {
        backgroundCollider = GetComponent<BoxCollider2D> ();
        backgroundHorizontalLength = backgroundCollider.size.x;
    }

    //Update runs once per frame
    private void Update()
    {
        if (transform.position.x < - transform.localScale.x * backgroundHorizontalLength)
        {
            RepositionBackground();
        }
    }

    //Moves the object this script is attached to right in order to create our looping background effect.
    private void RepositionBackground()
    {
        Vector2 groundOffSet = new Vector2(backgroundHorizontalLength * transform.localScale.x * 2.0f, 0.0f);

        transform.position = (Vector2) transform.position + groundOffSet;
    }
}