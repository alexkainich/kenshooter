﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIGameTimer : MonoBehaviour {

	void Update () 
	{
		GetComponent<UnityEngine.UI.Text>().text = "Time: " + Math.Round(Time.time, 2) + "s"; 
	}
}
