﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamLineAnimation : MonoBehaviour {

	private float targetDown = 0.2f;
	private float targetUp = 0.9f;
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		var test = gameObject.GetComponent<LineRenderer>();

		if (test.widthMultiplier == targetDown)
		{
			test.widthMultiplier = targetUp;
		}
		else
		{

			test.widthMultiplier = targetDown;
		}
	}
}
