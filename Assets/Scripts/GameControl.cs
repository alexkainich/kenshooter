﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    public static GameControl instance;

	public Text ScoreCount;

	[HideInInspector]
    public int Score;

	[HideInInspector]
    public bool gameOver = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

		DontDestroyOnLoad(gameObject);
    }

    public void PlayerDied()
    {
        gameOver = true;
    }

    public void AddToScore(int add)
    {
        Score += add;
        ScoreCount.text = "Score: " + Score;
    }
}
