﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerAttack : MonoBehaviour {

	private List<IManualWeapon> manualWeapons;

	void Start () 
	{
		manualWeapons = new List<IManualWeapon>();
		var manWps = gameObject.transform.Find("ManualWeapons");

		if (manWps != null)
		{
			foreach (Transform child in manWps.transform)
			{
				if (child.gameObject.activeSelf)
					manualWeapons.Add(child.gameObject.GetComponent<IManualWeapon>());
			}
		}
	}
	
	void Update ()
	{
		if (Input.GetButton("Fire"))
		{
			if (manualWeapons.Any())
			{
				manualWeapons.ForEach(mw => mw.Fire());
			}
		}
	}
}
