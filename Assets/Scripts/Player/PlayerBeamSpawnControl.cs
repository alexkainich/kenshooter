﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerBeamSpawnControl : MonoBehaviour, IManualWeapon
{

    public GameObject beamPrefab;
    public string[] ignoredTags;
    public GameObject startParticles;
    public GameObject endParticles;
    public LayerMask hitLayers;

    public int offsetParStartX;
    public int offsetParEndX;
    public int offsetBeamStartX;
    public int offsetBeamEndX;

    private GameObject beam;
    private CircleCollider2D beamCollider;
    private Vector2 hitPoint;
    private GameObject startPar;
    private GameObject endPar;

	private bool fired;

    void Start()
    {
        startPar = Instantiate(startParticles, startParticles.transform.position, Quaternion.identity);
        startPar.GetComponent<ParticleSystem>().Stop();
        endPar = Instantiate(endParticles, endParticles.transform.position, Quaternion.identity);
        endPar.GetComponent<ParticleSystem>().Stop();
		fired = false;
    }

    void Update()
    {
		if (!fired)
		{
			Destroy(beam);
			//Destroy(beamCollider);
			startPar.GetComponent<ParticleSystem>().Stop();
			endPar.GetComponent<ParticleSystem>().Stop();
		}
		else
		{
			fired = false;
		}
    }

    public void Fire()
    {
        InstantiateBeam();
        FindTarget();

        Vector3[] beamPoints = new Vector3[2];
        beamPoints[0] = new Vector3(transform.position.x + offsetBeamStartX, transform.position.y, 0);
        beamPoints[1] = new Vector3
        (
            hitPoint.x + offsetBeamEndX,
            transform.position.y,
            0
        );

        startPar.transform.position = new Vector2(beamPoints[0].x + offsetParStartX, beamPoints[0].y);
        endPar.transform.position = new Vector2(beamPoints[1].x + offsetParEndX, beamPoints[1].y);
        PlayStartParticles();
        PlayEndParticles();

        beam.GetComponent<LineRenderer>().SetPositions(beamPoints);
        AddColliderToBeam(beamPoints);

		fired = true;
    }

    void PlayStartParticles()
    {
        if (!startPar.GetComponent<ParticleSystem>().isPlaying)
        {
            startPar.GetComponent<ParticleSystem>().Play();
        }
    }

    void PlayEndParticles()
    {
        if (!endPar.GetComponent<ParticleSystem>().isPlaying)
        {
            endPar.GetComponent<ParticleSystem>().Play();
        }
    }

    void AddColliderToBeam(Vector3[] beamPoints)
    {
        GetCollider();

        beamCollider = beam.GetComponent<CircleCollider2D>();
        beamCollider.offset = new Vector2(beamPoints[1].x, beamPoints[1].y);
        beamCollider.radius = 1;
        beamCollider.isTrigger = true;
    }

    void GetCollider()
    {
        if (beamCollider == null)
        {
            beamCollider = beam.AddComponent<CircleCollider2D>();
        }
    }

    void InstantiateBeam()
    {
        if (beam == null)
        {
            beam = Instantiate(beamPrefab, beamPrefab.transform.position, Quaternion.identity);
        }
    }

    void FindTarget()
    {
        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, Vector2.right, 1000, hitLayers);

        List<RaycastHit2D> validTargets = hit.Where(h => !ignoredTags.Contains(h.collider.tag)).ToList();

        if (validTargets.Any())
        {
            validTargets.OrderBy(v => v.collider.transform.position.x);
            hitPoint = validTargets[0].point;
        }
        else
        {
            hitPoint = new Vector2(100, 100);
        }
    }
}
