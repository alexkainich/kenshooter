﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerShotSpawnControl : MonoBehaviour, IManualWeapon
{

    public GameObject shot;
    public float rotateSpriteZ = 0f;
    public ShotControl.ShotDirection shotDirection;
    public float rotateDegrees;
    public float fireRate;
    public bool isBeam;
    public bool asssignPlayerShotCollisionLayer;

    private float nextFire;
    private Vector2 shotDir;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Fire()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            var shotFired = Instantiate(shot, gameObject.transform.position, shot.transform.rotation);
            shotFired.GetComponent<ShotControl>().SetVelocity(shotDirection);
            if (rotateSpriteZ != 0) { shotFired.transform.Rotate(0f, 0f, rotateSpriteZ); }
            if (rotateDegrees != 0) { shotFired.GetComponent<Rigidbody2D>().transform.Rotate(0, 0, rotateDegrees); }
            if (asssignPlayerShotCollisionLayer) { shotFired.layer =  LayerMask.NameToLayer("PlayerShot"); }
            if (isBeam) { shotFired.tag = "PlayerBeam"; }
        }
        if (isBeam)
        {
            GameObject[] beamSprites = GameObject.FindGameObjectsWithTag("PlayerBeam");
            foreach (GameObject sprite in beamSprites)
            {
                sprite.transform.position = new Vector2(sprite.transform.position.x, gameObject.transform.position.y);
            }
        }
    }
}
