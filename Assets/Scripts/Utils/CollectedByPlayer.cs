﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectedByPlayer : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			GameControl.instance.AddToScore(100);
			Destroy(gameObject);
		}
	}
}
