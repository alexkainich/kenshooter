﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawnControl : MonoBehaviour {

    public Vector2 idleBeamWait;
    public Vector2 chargingBeamWait;
    public Vector2 laserBeamWait;

    public GameObject chargingBeam;
    public GameObject laserBeam;

    private GameObject chBeam;

    void Start ()
    {
        StartCoroutine(Attack());
    }
	
	void Update () {
        if (chBeam != null)
        {
            chBeam.transform.position = gameObject.transform.position;
            chBeam.transform.rotation = gameObject.transform.rotation;
        }
	}

    void OnDestroy()
    {
        if (chBeam != null)
            Destroy(chBeam);
    }

    void OnDisable()
    {
        if (chBeam != null)
            Destroy(chBeam);
    }

    IEnumerator Attack()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(idleBeamWait.x, idleBeamWait.y));

            chBeam = Instantiate(chargingBeam, gameObject.transform.position, gameObject.transform.rotation);
            yield return new WaitForSeconds(Random.Range(chargingBeamWait.x, chargingBeamWait.y));       
            Destroy(chBeam);

            chBeam = Instantiate(laserBeam, gameObject.transform.position, gameObject.transform.rotation);
            yield return new WaitForSeconds(Random.Range(laserBeamWait.x, laserBeamWait.y));
            Destroy(chBeam);
        }
    }
}
