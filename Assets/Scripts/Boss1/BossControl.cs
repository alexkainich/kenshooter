﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossControl : MonoBehaviour, IShipControl
{

    public float smoothing;
    public float speed;
    public Vector2 startWait;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;
    public Boundary boundary;

    private Transform playerTransform;

    private float currentSpeed;
    private float targetManeuver;
    private Rigidbody2D rb;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();

        var player = GameObject.FindGameObjectWithTag("Player");
        playerTransform = (player != null) ? player.transform : null;

        if (playerTransform != null)
            StartCoroutine(FollowPlayerY());
    }

    // Evade method
    IEnumerator FollowPlayerY()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

        while(true)
        {
            targetManeuver = playerTransform.position.y;
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = gameObject.transform.position.y;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Vector2 bossPosition = gameObject.transform.position;
        int direction = (targetManeuver == bossPosition.y) ? 0 : (targetManeuver > bossPosition.y) ? 1 : -1 ;

        gameObject.transform.position = new Vector2(bossPosition.x, bossPosition.y + direction * speed * Time.deltaTime);

	}

    public void PushBackShip(float collisionForce, Vector2 pusherPosition)
    {
        Vector2 forceVector = ((Vector2)gameObject.transform.position - pusherPosition).normalized;
        rb.AddForce(forceVector * collisionForce);
    }
}
