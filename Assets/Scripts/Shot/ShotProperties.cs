﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


class ShotProperties : MonoBehaviour
{
    public float damage = 0;
    public float damageEverySeconds = 5;
    public float pushBackforce = 0;
}

