﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ShotControl : MonoBehaviour
{
    public float shotSpeed;
    public ShotDirection shotDirection = ShotDirection.none;
    public float destroyByTime;
    public bool explodeOnDestroyedByTime = false;
    public bool explodeOnImpact = true;
    public bool destroyOnImpact = true;
    public float rotationSpeed = 0;
    public float rotationSmooth = 1f;

    public GameObject shotExplosion;
    public bool autoAssignExplosionCollisionLayer = true;
    public string[] ignoredTags;

    private Quaternion targetRotation;

    void Awake()
    {
        targetRotation = transform.rotation;

        SetVelocity(shotDirection);
        Destroy(gameObject, destroyByTime);
    }

    void FixedUpdate()
    {
        if (rotationSpeed != 0)
        {
            targetRotation *= Quaternion.AngleAxis(60, Vector3.forward);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * rotationSmooth * Time.deltaTime);
        }
    }

    void OnDestroy()
    {
        if (explodeOnDestroyedByTime)
        {
            var shotExpl = Instantiate(shotExplosion, transform.position, Quaternion.identity);
            Destroy(shotExpl, 2f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!ignoredTags.Contains(collider.gameObject.tag))
        {
            if (explodeOnImpact) 
                 InstantiateExplosion();
            if (destroyOnImpact)
                Destroy(gameObject);
        }
    }

    private void InstantiateExplosion()
    {
        var shotExpl = Instantiate(shotExplosion, transform.position, Quaternion.identity);

        if (autoAssignExplosionCollisionLayer)
        {
            if (gameObject.layer == LayerMask.NameToLayer("PlayerShot"))
                shotExpl.layer = LayerMask.NameToLayer("PlayerExplosion");
            else if (gameObject.layer == LayerMask.NameToLayer("EnemyShot"))
                shotExpl.layer = LayerMask.NameToLayer("EnemyExplosion");
        }

        float explosionClipLength = shotExpl.GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length;
        Destroy(shotExpl, explosionClipLength);
    }

    public void SetVelocity(ShotDirection shotDirection)
    {
        switch (shotDirection)
        {
            case ShotDirection.right:
                GetComponent<Rigidbody2D>().velocity = GetComponent<Transform>().right * shotSpeed;
                break;
            case ShotDirection.left:
                GetComponent<Rigidbody2D>().velocity = -GetComponent<Transform>().right * shotSpeed;
                break;
            case ShotDirection.up:
                GetComponent<Rigidbody2D>().velocity = GetComponent<Transform>().up * shotSpeed;
                break;
            case ShotDirection.down:
                GetComponent<Rigidbody2D>().velocity = -GetComponent<Transform>().up * shotSpeed;
                break;
            default:
                break;
        }
    }

    public enum ShotDirection
    {
        right = 1,
        left = -1,
        up = 2,
        down = -2,
        none = 3
    }
}
