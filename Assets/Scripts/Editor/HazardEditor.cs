using UnityEngine;
using System;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Hazard), true), CanEditMultipleObjects]
public class HazardEditor : Editor
{
    public FormationType formation_Type;

    private bool showPortalFields = true;
    private bool showHazardFields = true;
    private bool showHazardTypeFields = true;
    private bool showFormationTypeFields = true;

    public override void OnInspectorGUI()
    {
        Hazard hazard = (Hazard)target;

        //Portal
        showPortalFields = EditorGUILayout.Foldout(showPortalFields, "Portal");
        if (showPortalFields)
        {
            using (var group = new EditorGUILayout.FadeGroupScope(Convert.ToSingle(false)))
            {
                EditorGUI.indentLevel++;
                hazard.portalType = (PortalType)EditorGUILayout.EnumPopup("Type", hazard.portalType);
                hazard.portalCenter = (Vector2)EditorGUILayout.Vector2Field("Center", hazard.portalCenter);
                hazard.portalSize = (Vector2)EditorGUILayout.Vector2Field("Size", hazard.portalSize);
                hazard.portalOpen = (float)EditorGUILayout.FloatField("Open", hazard.portalOpen);
                hazard.portalClose = (float)EditorGUILayout.FloatField("Close", hazard.portalClose);
                EditorGUI.indentLevel--;
            }
        }

        //Hazard Type
        showHazardTypeFields = EditorGUILayout.Foldout(showHazardTypeFields, "Hazard Type");
        if (showHazardTypeFields)
        {
            EditorGUI.indentLevel++;
            DrawDefaultInspector();
            EditorGUI.indentLevel--;          
        }

        //Hazard
        showHazardFields = EditorGUILayout.Foldout(showHazardFields, "Hazard");
        if (showHazardFields)
        {
            using (var group = new EditorGUILayout.FadeGroupScope(Convert.ToSingle(false)))
            {
                EditorGUI.indentLevel++;
                hazard.disable = (bool)EditorGUILayout.Toggle("Disable", hazard.disable);
                hazard.prefab = (GameObject)EditorGUILayout.ObjectField(hazard.prefab, typeof(GameObject), false);
                hazard.memberCount = (int)EditorGUILayout.IntField("Member Count", hazard.memberCount);
                
                ShowFormationFields(hazard);
                EditorGUI.indentLevel--;
            }
        }      
    }

    private void ShowFormationFields(Hazard hazard)
    {
        showFormationTypeFields = EditorGUILayout.Foldout(showFormationTypeFields, "Formation");
        if (showFormationTypeFields)
        {
            using (var group = new EditorGUILayout.FadeGroupScope(Convert.ToSingle(false)))
            {
                if (group.visible == false)
                {
                    EditorGUI.indentLevel++;
                    hazard.formationType = (FormationType)EditorGUILayout.EnumPopup("Type", hazard.formationType);

                    //Formation Type
                    formation_Type = (FormationType)serializedObject.FindProperty("formationType").enumValueIndex;
                    switch (formation_Type)
                    {
                        case FormationType.Line:
                            {
                                using (var group2 = new EditorGUILayout.FadeGroupScope(Convert.ToSingle(hazard.formationType != FormationType.Line)))
                                {
                                    if (group2.visible == false)
                                    {
                                        EditorGUI.indentLevel++;
                                        ((WaveHazard)hazard).vectorToNext = EditorGUILayout.Vector2Field("Vector to next", ((WaveHazard)hazard).vectorToNext);
                                        EditorGUI.indentLevel--;
                                    }
                                }

                            }
                            break;
                        default:
                            {

                            }
                            break;
                    }
                    EditorGUI.indentLevel--;
                }
            }
        }
    }
}