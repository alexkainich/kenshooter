using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SceneControl))]
public class SceneBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        SceneControl sceneControl = (SceneControl)target;
        if(GUILayout.Button("Add New Hazard"))
        {
            sceneControl.AddHazard();
        }
    }
}