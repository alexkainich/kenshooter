using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public abstract class Hazard : MonoBehaviour
{
    //Hazard
    [HideInInspector]
    public bool disable = false;
    [HideInInspector]
    public GameObject prefab;
    [HideInInspector]
    public int memberCount = 2;

    //Portal
    [HideInInspector]
    public PortalType portalType = PortalType.Flat;
    [HideInInspector]
    public Vector2 portalCenter = new Vector2(44, 0);
    [HideInInspector]
    public Vector2 portalSize = new Vector2(0, -22);
    [HideInInspector]
    public float portalOpen = 1.0f;
    [HideInInspector]
    public float portalClose = Mathf.Infinity;

    //Formation (needs own class?)
    [HideInInspector]
    public FormationType formationType = FormationType.Random;
    [HideInInspector]
    public Vector2 vectorToNext;
}