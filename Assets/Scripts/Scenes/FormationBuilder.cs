using UnityEngine;

// Consider adding an instance (non static) and adding prefab, etc
public static class FormationBuilder {
    public static Vector2[] RandomFormation(GameObject prefab, int memberCount, Vector2 portalSize)
    {
        Vector2[] formationPositions = new Vector2[memberCount];

        for (int i = 0; i < memberCount; i++)
        {
            formationPositions[i] = new Vector2(Random.Range(-1.0f, 1.0f) * portalSize.x, Random.Range(-1.0f, 1.0f) * portalSize.y);
        }

        return formationPositions;
    }

    public static Vector2[] LineFormation(GameObject prefab, int memberCount, Vector2 vectorToNext, Vector2 portalSize)
    {
        Vector2[] positions = new Vector2[memberCount];

        Vector2 portalRandomPos = Random.Range(-1.0f, 1.0f) * portalSize;

        for (int i = 0; i < memberCount; i++)
        {
            positions[i] = new Vector2(portalRandomPos.x + i * vectorToNext.x, portalRandomPos.y + i * vectorToNext.y);
        }

        return positions;
    }

    public static Vector2[] GetFormations(FormationType formationType, GameObject prefab, int memberCount, Vector2 vectorToNext, Vector2 portalSize)
    {
        switch (formationType)
        {
            case FormationType.Line:
                return LineFormation(prefab, memberCount, vectorToNext, portalSize);
            case FormationType.Random:
                return RandomFormation(prefab, memberCount, portalSize);
            default:
                return RandomFormation(prefab, memberCount, portalSize);
        }
    }
}