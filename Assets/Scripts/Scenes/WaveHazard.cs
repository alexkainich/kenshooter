using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaveHazard : Hazard
{
    public int numOfWaves = 2;
    public float gapBetweenWaves = 5.0f;

    void Awake()
    {
        if (disable) return;
        
        StartCoroutine(GetEnemies());
    }

    IEnumerator GetEnemies()
    {
        yield return new WaitForSeconds(portalOpen);

        for (int i = 0; i < numOfWaves - 1; i++)
        {
            if (Time.time >= portalClose)
                yield break;

            foreach (Vector2 relPos in FormationBuilder.GetFormations(formationType, prefab, memberCount, vectorToNext, portalSize))
            {
                // Assuming 0 transform from prefab
                Instantiate(this.prefab, portalCenter + relPos, this.prefab.transform.rotation);
            }

            yield return new WaitForSeconds(gapBetweenWaves);
        }
    }
}