using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SingleHazard : Hazard
{
    void Awake()
    {
        if (disable) return;

        StartCoroutine(GetEnemies());
    }

    IEnumerator GetEnemies()
    {
        yield return new WaitForSeconds(portalOpen);

        foreach (Vector2 relPos in FormationBuilder.GetFormations(FormationType.Random, prefab, memberCount, new Vector2(0, 0), portalSize))
        {
            // Assuming 0 transform from prefab
            Instantiate(this.prefab, portalCenter + relPos, this.prefab.transform.rotation);
        }
    }
}