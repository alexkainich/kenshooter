﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class SceneControl : MonoBehaviour
{
    public static SceneControl instance;
    public float scrollSpeed = -1.5f;
    public float endSceneByTime = Mathf.Infinity;
    public HazardType newHazardType = HazardType.Single;
    
    private bool sceneEnded = false;

    public void AddHazard()
    {
        var newHazard = new GameObject();
        newHazard.name = "NewHazard";
        newHazard.transform.parent = gameObject.transform;

        switch(newHazardType)
        {
            case HazardType.Single:
                newHazard.AddComponent<SingleHazard>();
                break;
            case HazardType.Wave:
                newHazard.AddComponent<WaveHazard>();
                break;
            default:
                throw new UnityException("AddHazard: Cannot add hazard of this type.");
        }
    }

    void Awake () {
        // Should be singleton
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
	}
}
